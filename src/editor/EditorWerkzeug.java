package editor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;

public class EditorWerkzeug
{
	private EditorWerkzeugUI _ui;

	public EditorWerkzeug()
	{
		_ui = new EditorWerkzeugUI();
		registriereUIAktionen();
	}

	public void registriereUIAktionen()
	{
		_ui.getNewFileItem().addActionListener(e -> clearText());
		_ui.getLoadItem().addActionListener(e -> loadFile());
		_ui.getSaveItem().addActionListener(e -> saveFile());
		_ui.getExitItem().addActionListener(e -> System.exit(1));
	}

	/**
	 * �ffnet einen FileChooser Dialog und l�dt die ausgew�hlte Datei in den Editor
	 */
	private void loadFile() 
	{
		_ui.getFileChooser().setDialogType(JFileChooser.OPEN_DIALOG);
		int value = _ui.getFileChooser().showDialog(null, "Datei laden");
		
		if(value == JFileChooser.APPROVE_OPTION)
		{
		    File path = _ui.getFileChooser().getSelectedFile();
			String text = FileOperations.readFile(path);
			_ui.getTextArea().setText(text);
		}
	}

	/**
	 * �ffnet einen FileChooser Dialog und speichert den Inhalt des Editors an der ausew�hlten Stelle
	 */
	private void saveFile()
	{
		_ui.getFileChooser().setDialogType(JFileChooser.SAVE_DIALOG);
		int value = _ui.getFileChooser().showDialog(null, "Datei speichern");
		
		if (value == JFileChooser.APPROVE_OPTION) 
		{
			File path = _ui.getFileChooser().getSelectedFile();
			FileOperations.writeFile(path, _ui.getTextArea().getText());
		}
	}

	/**
	 * Entfernt den Inhalt des Editors
	 */
	public void clearText()
	{
		_ui.getTextArea().setText("");
	}
}