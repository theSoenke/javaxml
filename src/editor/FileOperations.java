package editor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileOperations
{
	/**
	 * @param path Pfad zur Datei
	 * @return Gibt den Inhalt der Datei aus dem �bergebeben Pfad zur�ck
	 */
	public static String readFile(File path)
	{
		String content = "";
		try(FileReader file = new FileReader(path)) 
		{
			BufferedReader reader = new BufferedReader(file);
			String line;

			while ((line = reader.readLine()) != null) 
			{
				content += line + "\n";
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return content;
	}

	/**
	 * @param path
	 *            Pfad zur Datei
	 * @param content
	 *            Inhalt der Datei
	 */
	public static void writeFile(File path, String content)
	{
		try (FileWriter output = new FileWriter(path))
		{
			output.write(content);
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}