package editor;

import java.awt.BorderLayout;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class EditorWerkzeugUI
{
	private JFrame _frame;
	private JTextArea _textArea;
	private JMenuBar _menuBar;
	private JMenu _menuFile;
	private JMenuItem _loadItem;
	private JMenuItem _saveItem;
	private JMenuItem _exitItem;
	private JMenuItem _newFileItem;
	private JFileChooser _fileChooser;

	private final int WIDTH = 720;
	private final int HEIGHT = 720;

	public EditorWerkzeugUI()
	{
		InitUI();
	}

	private void InitUI()
	{
		_textArea = new JTextArea();
		_textArea.setSize(1000, 1000);

		_menuBar = new JMenuBar();
		_menuFile = new JMenu("Datei");

		_newFileItem = new JMenuItem("Neue Datei");
		_loadItem = new JMenuItem("Öffnen");
		_saveItem = new JMenuItem("Speichern");
		_exitItem = new JMenuItem("Beenden");

		_menuBar.add(_menuFile);

		_menuFile.add(_newFileItem);
		_menuFile.add(_loadItem);
		_menuFile.add(_saveItem);
		_menuFile.add(_exitItem);

		_fileChooser = new JFileChooser();

		_frame = new JFrame("File Editor");
		_frame.setSize(WIDTH, HEIGHT);
		_frame.setLayout(new BorderLayout());
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_frame.setLocationRelativeTo(null);
		_frame.setVisible(true);

		_frame.getContentPane().add(_menuBar, BorderLayout.NORTH);
		_frame.add(new JScrollPane(_textArea));
	}

	public JFileChooser getFileChooser()
	{
		return _fileChooser;
	}

	public JMenuItem getNewFileItem()
	{
		return _newFileItem;
	}

	public JMenuItem getLoadItem()
	{
		return _loadItem;
	}

	public JMenuItem getSaveItem()
	{
		return _saveItem;
	}

	public JMenuItem getExitItem()
	{
		return _exitItem;
	}

	public JTextArea getTextArea()
	{
		return _textArea;
	}
}