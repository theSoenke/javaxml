package xml;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;


public class SAXparser
{
public static void main(String[] args)
{
	try
	{
		
	//Erzeugen einer InputSource inklusive Filereader
	XMLReader xmlReader = XMLReaderFactory.createXMLReader();
	FileReader fileReader = new FileReader("beispielXML.xml");
	InputSource inputSource = new InputSource(fileReader);
	
	//Erzeugen eines Handlers und parsing
	xmlReader.setContentHandler(new RaumHandler());
	xmlReader.parse(inputSource);
	}
	
	catch(FileNotFoundException e)
	{
		e.printStackTrace();
	}
	catch(IOException e)
	{
		e.printStackTrace();
	}
	catch(SAXException e)
	{
		e.printStackTrace();
	}
}
}
