package xml;

/**
 * Sehr triviale Klasse eines Raums mit einem Namen. 
 * @author Simon Weidmann
 *
 */
public class Raum
{
	private String _name;
	
	public Raum(String name)
	{
		_name = name;
	}
	
	public String getName()
	{
		return _name;
	}
	
	public void setName(String name)
	{
		_name = name;
	}
	
	@Override
	public String toString()
	{
		if(_name == null)
			{return "Ein namenloser Raum";}
		
		else
		{		
			return _name;
		}
	}

}
