package xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLSchreiber
{
	public static void main(String[] args)
	{
		try
		{

			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory
					.newDocumentBuilder();

			Document document = documentBuilder.newDocument();
			
			
			//erzeugt Haus, Raum und Mensch und fügt sie zum Document zu.
			Element hauselement = document.createElement("Haus");
			document.appendChild(hauselement);
			Element raumelement = document.createElement("Raum");
			raumelement.setAttribute("Wandfarbe", "blau gepunktet");
			hauselement.appendChild(raumelement);
			Element menschelement = document.createElement("Mensch");
			menschelement.setTextContent("42 Jahre alt");
			raumelement.appendChild(menschelement);
			
			//erzeugt Quelle und Result
			DOMSource domSource = new DOMSource(document);
			File outputFile = new File("erzeugtesXML.xml");
			StreamResult streamResult = new StreamResult(outputFile);
			
			//Transformation mit UTF-8 Encoding und Einrückungen
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();			
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(domSource, streamResult);

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
