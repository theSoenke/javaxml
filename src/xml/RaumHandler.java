package xml;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;




/**
 * Handler, der auf die Elemente eines XML-Files reagiert.
 * @author Simon W.
 *
 */
public class RaumHandler implements ContentHandler
{
	private String _currentvalue;
	private ArrayList<Raum> _raumliste;
	private Raum _aktuellerRaum;
	
	/**
	 * Hält den text zwischen den aktuellen Tags fest.
	 */
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException
	{

		_currentvalue = new String(ch, start, length);
	}

	/**
	 * Bei Start des Dokuments wird eine leere Raumliste erzeugt
	 */
	@Override
	public void startDocument() throws SAXException
	{
		_raumliste = new ArrayList<Raum>();
		System.out.println("Es gibt folgende Räume:");
	}
	
	/**
	 * Beim Lesen des Tags "raum" wird ein neuer Raum erzeugt.
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException
	{
		if(localName.equals("raum"))
		{
			_aktuellerRaum = new Raum(null);
		}
	}
	
	/**
	 * Beim Lesen des Endtags von "name" wird dem aktuellen Raum der Name im _currentvalue gegeben.
	 * Beim Lesen des Endtags von "raum" wird der Raum der Raumliste zugefügt.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException
	{
		if (localName.equals("name"))
		{
			_aktuellerRaum.setName(_currentvalue);;
		}
		
		if (localName.equals("raum"))
		{
			_raumliste.add(_aktuellerRaum);
		}
	}
	
	/**
	 * Zum Schluss werden alle Räume aus der Liste aufgelistet.
	 */
	@Override
	public void endDocument() throws SAXException
	{
		for(int x = 0; x < _raumliste.size(); x++)
		{
			System.out.println(_raumliste.get(x).toString());
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public void startPrefixMapping(String arg0, String arg1)
			throws SAXException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2)
			throws SAXException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String arg0, String arg1)
			throws SAXException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException
	{
		// TODO Auto-generated method stub

	}


}
