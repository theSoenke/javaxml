package xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMparser
{
	public static void main(String[] args)
	{
		try
		{
			//Erzeugen des Files und des XML-Documents.
			File xmlFile = new File("beispielXML.xml");  // Pfad der XML-Datei
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document document = dBuilder.parse(xmlFile);
			
			//Erzeugen einer List aller Nodes (Tags und Text dazwischen)
			NodeList nList = document.getElementsByTagName("raum");
			
			System.out.println("Es gibt folgende Räume:");
			
			for (int x = 0; x < nList.getLength(); x++)
			{
				Node aktNode = nList.item(x);
				Element aktElement = (Element) aktNode;
				//Ausgabe des Textinhalts aller Elemente mit dem Tag "name"
				System.out.println("-" + aktElement.getElementsByTagName("name")
						.item(0).getTextContent());
			}				

		}

		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
